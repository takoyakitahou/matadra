package mataDra.logics.items;

import mataDra.dao.items.WeaponDAO;
import mataDra.entity.items.equip.WeaponEntity;

public class WeaponListLogic extends ItemListLogic<WeaponEntity> {

	@Override
	public WeaponEntity getItem(int index) {
		// TODO 自動生成されたメソッド・スタブ
		WeaponDAO dao = new WeaponDAO();
		return dao.findByIndex(index);
	}

}
