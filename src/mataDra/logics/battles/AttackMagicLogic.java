package mataDra.logics.battles;

import java.util.List;

import mataDra.entity.ability.MagicEntity;
import mataDra.entity.creatures.Character;
import mataDra.view.ui.MessageArea;
import mataDra.viewss.ui.ImagePlayer;
import mataDra.viewss.ui.SoundPlayer;

/**
 * 呪文攻撃
 *
 *
 */
public class AttackMagicLogic extends AttackLogic {

    /**
     * 攻撃設定
     *
     * @param offenceCharacter
     * @param abilityIndex
     * @param defenceCharacter
     * @param partyIndex
     */
    public void setAttack(Character oc, int abilityIndex, List<Character> dcs) {
        Character dc;// ディフェンスキャラクター
        String msg = "";// メッセージ
        int bonus;// ステータスボーナス
        int point;// 攻撃力
        MagicEntity magic;// 呪文クラス
        int attack;// 総合攻撃力
        int defence;// 総合防御力
        int hit;// 命中率
        boolean isMiss;// 命中判定
        int damage;// 最終ダメージ

        // ディフェンスパーティの数だけ繰り返す。
        for (int i = 0; i < dcs.size(); i++) {
            dc = dcs.get(i);
            magic = oc.getMagics().get(abilityIndex);
            bonus = getBonus(oc.getIntelligence());
            point = magic.getPoint();
            attack = calcTotalAttack(oc.getIntelligence(), bonus, point);
            defence = calcTotalDefence(dc.getIntelligence(), 0);
            hit = calcAttackHitRate(oc.getIntelligence(), dc.getIntelligence());
            isMiss = isAttackMiss(hit);
            damage = calcTotalDamage(attack, defence);

            // 攻撃開始
            msg = oc.getName() + "は" + magic.getName() + "のじゅもんをとなえた。";
            MessageArea.message(msg);
            ImagePlayer.play(magic.getImage());
            SoundPlayer.play(magic.getSound());
            AttackDamageMessage(dc, damage, isMiss);
            deadMessage(dc);
        }
    }
}
