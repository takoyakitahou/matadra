package mataDra.dao.sounds;

import java.util.List;

import mataDra.dao.DAO;
import mataDra.entity.sounds.SoundEntity;
import mataDra.entity.sounds.SoundEntity.SoundType;

public class SoundDAO extends DAO<SoundEntity> {

	@Override
	public List<SoundEntity> registerAll() {
		// TODO 自動生成されたメソッド・スタブ
		setEntity(new SoundEntity(0, "単体攻撃1", "バシッ!", SoundType.PLAYER_HIT));
		setEntity(new SoundEntity(1, "単体攻撃2", "ザシュッ!", SoundType.ENEMY_HIT));
		setEntity(new SoundEntity(2, "単体攻撃1", "ドカッ!", SoundType.ENEMY_HIT));
		setEntity(new SoundEntity(3, "ミス", "サッ", SoundType.MISS));
		setEntity(new SoundEntity(4, "攻撃無効", "キンッ", SoundType.NO_DAMAGE));
		setEntity(new SoundEntity(5, "クリティカル攻撃1", "バッシーーーン!", SoundType.PLAYER_CRITICAL));
		setEntity(new SoundEntity(6, "クリティカル攻撃2", "ドッゴーーーン!", SoundType.ENEMY_CRITICAL));
		setEntity(new SoundEntity(7, "一撃必殺", "プシュッ", SoundType.ASSASINATE));
		setEntity(new SoundEntity(8, "フィールドBGM", "テーレーレー", SoundType.FIELD));
		setEntity(new SoundEntity(9, "戦闘BGM", "チャーンチャラチャラチャーン", SoundType.BATTLE));
		setEntity(new SoundEntity(10, "ロビーBGM", "チャララッチャララッ", SoundType.TOWN));
		setEntity(new SoundEntity(11, "レベルアップ", "テレッテッテー", SoundType.LEVEL_UP));
		setEntity(new SoundEntity(12, "宿泊", "チャララチャッチャッチャーン", SoundType.INN));

		return getEntity();

	}

}
